﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class RoleItemResponse
    {
        public RoleItemResponse(){}

        public RoleItemResponse(Role role)
        {
            Id = role.Id;
            Name = role.Name;
            Description = role.Description;
        }

        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Название роли
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание роли
        /// </summary>
        public string Description { get; set; }
    }
}