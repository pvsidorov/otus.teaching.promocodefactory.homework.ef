﻿using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CreateOrUpdateRoleRequest
    {
        
        /// <summary>
        /// Наименование роли
        /// </summary>
        [Required, MaxLength(20)]
        public string Name { get; set; }

        /// <summary>
        /// Описание роли
        /// </summary>
        [MaxLength(200)]
        public string Description { get; set; }

        /// <summary>
        /// Список сотрудников для данной роли
        /// </summary>
        public IEnumerable<Guid> EmployeeIds { get; set; }

        public Role ToRole()
        {
            return new Role
            {
                Description = this.Description,
                Name = this.Name
            };
        }
    }
}
