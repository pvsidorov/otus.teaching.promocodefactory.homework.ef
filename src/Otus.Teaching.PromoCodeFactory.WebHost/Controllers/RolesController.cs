﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController:ControllerBase
    {
        private readonly IRepository<Role> _rolesRepository;
        private readonly IRepository<Employee> _employeesRepository;

        public RolesController(IRepository<Role> rolesRepository, IRepository<Employee> employeesRepository)
        {
            _rolesRepository = rolesRepository;
            _employeesRepository = employeesRepository;
        }
        
        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetRolesAsync()
        {
            try
            {
                var roles = await _rolesRepository.GetAllAsync();
                return Ok(roles.Select(x => new RoleItemResponse(x)).ToList());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        /// <summary>
        /// Добавляет новую роль
        /// </summary>
        /// <remarks>
        /// METHOD POST:
        /// {
        ///     "name": "Seller",
        ///     "description": "Продавец",
        ///     "employeeId": []
        /// }
        /// </remarks>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(Role),201)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(DbUpdateException), 422)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> AddRoleAsyc(CreateOrUpdateRoleRequest request)
        {
            if (request is null || !ModelState.IsValid)
                return BadRequest();

            try
            {
                var employees = (await _employeesRepository.GetListIdAsync(request.EmployeeIds)).ToList();
                var role = request.ToRole();

                if (employees.Any())
                    role.Employees.AddRange(employees);
                await _rolesRepository.AddAsync(role);
                return StatusCode(201, role);

            }
            catch (DbUpdateException ex)
            {
                return StatusCode(422, $"Error{ex.Message}\n{ex.InnerException?.Message}");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error{ex.Message}\n{ex.InnerException?.Message}");
            }
        }
    }
}