﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.DataContext
{
    /// <summary>
    /// Контекст БД приложения
    /// </summary>
    public sealed class AppDbContext:DbContext
    {
        /// <summary>
        /// Коструктор по умолчанию
        /// </summary>
        /// <param name="options"></param>
        public AppDbContext(DbContextOptions<AppDbContext> options):base(options) 
        {
        //    Database.EnsureDeleted();
        //    Database.EnsureCreated();
        }

        /// <summary>
        /// Таблица промокодов
        /// </summary>
        public DbSet<PromoCode> PromoCodes { get; set; }

        /// <summary>
        /// Таблтца клиентов
        /// </summary>
        public DbSet<Customer> Customers { get; set; }

        /// <summary>
        /// Таблица привелегий (справочник)
        /// </summary>
        public DbSet<Preference> Preferences { get; set; }
        
        /// <summary>
        /// Таблица сотрудников
        /// </summary>
        public DbSet<Employee> Employees { get; set; }
        
        /// <summary>
        /// Таблица ролей
        /// </summary>
        public DbSet<Role> Roles { get; set; }
        
        /// <summary>
        /// Таблица привелегий клиентов
        /// </summary>
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        /// <summary>
        /// Действия при создании модели
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerPreference>()
                .HasKey(x => new { x.CustomerId, x.PreferenceId });

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(x => x.Customer)
                .WithMany(x => x.CustomerPreferences)
                .HasForeignKey(x => x.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(x => x.Preference)
                .WithMany(x => x.CustomerPreferences)
                .HasForeignKey(x => x.PreferenceId);

            //Поле Name в таблице Role уникально
            modelBuilder.Entity<Role>().HasIndex(x => x.Name).IsUnique();

            //Заливка фейковыми данными
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
           
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            modelBuilder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomerPreferences);
        }
 
    }
}
