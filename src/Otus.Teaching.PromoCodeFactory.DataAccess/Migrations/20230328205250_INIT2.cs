﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class INIT2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "CustomerPreferences",
                keyColumns: new[] { "CustomerId", "PreferenceId" },
                keyValues: new object[] { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") },
                column: "Id",
                value: new Guid("2d698c89-4a8c-47f0-b15b-9f00108fe625"));

            migrationBuilder.UpdateData(
                table: "CustomerPreferences",
                keyColumns: new[] { "CustomerId", "PreferenceId" },
                keyValues: new object[] { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f1"), new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c") },
                column: "Id",
                value: new Guid("a6e9ba23-c361-45c4-8ebf-f5d1a4f574e2"));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "CustomerPreferences",
                keyColumns: new[] { "CustomerId", "PreferenceId" },
                keyValues: new object[] { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") },
                column: "Id",
                value: new Guid("82c3f3d8-dbd0-4e83-9503-faf074c104f8"));

            migrationBuilder.UpdateData(
                table: "CustomerPreferences",
                keyColumns: new[] { "CustomerId", "PreferenceId" },
                keyValues: new object[] { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f1"), new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c") },
                column: "Id",
                value: new Guid("787c5ae7-99ef-4f68-92b9-f9cc0d08be7d"));
        }
    }
}
