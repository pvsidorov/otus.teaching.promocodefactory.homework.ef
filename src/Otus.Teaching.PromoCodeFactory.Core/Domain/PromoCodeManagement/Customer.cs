﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    /// <summary>
    /// Клиент
    /// </summary>
    public class Customer : BasePerson
    {
        /// <summary>
        /// Список привилегий
        /// </summary>
        [ForeignKey("PreferencesId")]
        public IEnumerable<CustomerPreference> CustomerPreferences { get; set; }

        /// <summary>
        /// Список промокодов
        /// </summary>
        [ForeignKey("PromoCodesId")]
        public IEnumerable<PromoCode> PromoCodes { get; set; }
    }
}