﻿using System;
using System.ComponentModel.DataAnnotations;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    /// <summary>
    /// Промокод
    /// </summary>
    public class PromoCode : BaseEntity
    {
        /// <summary>
        /// Код промокода
        /// </summary>
        [Required, MaxLength(20), DataType(DataType.Text)]
        public string Code { get; set; }

        /// <summary>
        /// Сервисная информация
        /// </summary>
        [MaxLength(200), DataType(DataType.MultilineText)]
        public string ServiceInfo { get; set; }

        /// <summary>
        /// Дата начала действия
        /// </summary>
        [Required, DataType(DataType.Date)]
        public DateTime BeginDate { get; set; }

        /// <summary>
        /// Дата окончания действия
        /// </summary>
        [Required, DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Наименование партнера
        /// </summary>
        [Required, MaxLength(150), DataType(DataType.Text)]
        public string PartnerName { get; set; }

        /// <summary>
        /// Ответсвенный сотрудник по промокоду
        /// </summary>
        [Required]
        public Employee PartnerManager { get; set; }

        /// <summary>
        /// Привилегия 
        /// </summary>
        [Required]
        public Preference Preference { get; set; }
    }
}