﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    /// <summary>
    /// Привилегии
    /// </summary>
    public class Preference:BaseEntity
    {
        /// <summary>
        /// Наименование привилегии
        /// </summary>
        [MinLength(3), MaxLength(30), Required]
        public string Name { get; set; }

        /// <summary>
        /// Привилегии
        /// </summary>
        public IEnumerable<CustomerPreference> CustomerPreferences { get; set; }

        /// <summary>
        /// Промокоды по данной привилегии
        /// </summary>
        public IEnumerable<PromoCode> PromoCodes { get; set; }   
    }
}