﻿using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{

    /// <summary>
    /// Роль 
    /// </summary>
    public class Role : BaseEntity
    {
        /// <summary>
        /// Наименование роли
        /// </summary>
        [Required, MaxLength(20)]
        public string Name { get; set; }

        /// <summary>
        /// Описание роли
        /// </summary>
        [MaxLength(200)]
        public string Description { get; set; }

        /// <summary>
        /// Ключ для связки с таблицей ролей
        /// </summary>
        public Guid EmployeeId { get; set; }

        /// <summary>
        /// Список сотрудников
        /// </summary>
        public virtual List<Employee> Employees { get; set; } = new();
    }
}