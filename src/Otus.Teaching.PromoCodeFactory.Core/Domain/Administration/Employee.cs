﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{

    /// <summary>
    /// Сотрудник
    /// </summary>
    public class Employee:BasePerson
    {
        /// <summary>
        /// ID роли сотрудника
        /// </summary>
        public Guid RoleId { get; set; }
        /// <summary>
        /// Роль
        /// </summary>
        [ForeignKey("RoleId")]
        public Role Role { get; set; }

        /// <summary>
        /// Примененные промокоды (Количество)
        /// </summary>
        public int AppliedPromocodesCount { get; set; }
    }
}