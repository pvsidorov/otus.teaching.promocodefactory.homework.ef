﻿using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    /// <summary>
    /// Обобщенное описание персоны
    /// </summary>
    public abstract class BasePerson:BaseEntity
    {
        /// <summary>
        /// Имя 
        /// </summary>
        [MaxLength(50)]
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        [MaxLength(50)]
        public string LastName { get; set; }

        /// <summary>
        /// Адрес электронной почты
        /// </summary>
        [MaxLength(50), DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        /// <summary>
        /// Полное имя
        /// </summary>
        public string FullName => $"{FirstName} {LastName}";

    }
}
