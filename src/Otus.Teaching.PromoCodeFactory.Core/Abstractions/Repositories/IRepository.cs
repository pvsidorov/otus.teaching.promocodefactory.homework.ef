﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    /// <summary>
    /// Абстракция для репозитория
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T: BaseEntity
    {
        /// <summary>
        /// Получить весь список сущностей
        /// </summary>
        /// <returns>Сокращенный список сущностей</returns>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Получить список сущностей по их идентификаторам
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        Task<IEnumerable<T>> GetListIdAsync(IEnumerable<Guid> listId);

        /// <summary>
        /// Получить сущность по ID
        /// </summary>
        /// <param name="id">Идентификатор сущности</param>
        /// <returns>Полное описание сущности</returns>
        Task<T> GetByIdAsync(Guid id);

        /// <summary>
        /// Добавить сущность
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <returns></returns>
        Task AddAsync(T entity);

        /// <summary>
        /// Обновить сущность
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <returns></returns>
        Task UpdateAsync(T entity);

        /// <summary>
        /// Удалить сущность
        /// </summary>
        /// <param name="entity">Сущность</param>
        /// <returns></returns>
        Task DeleteAsync(T entity);

        /// <summary>
        /// Удалить сущность
        /// </summary>
        /// <param name="id">Идентификатор сущности</param>
        /// <returns></returns>
        Task DeleteByIdAsync(Guid id);
    }
}